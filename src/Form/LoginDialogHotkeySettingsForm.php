<?php

namespace Drupal\login_dialog_hotkey\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Define a form for module configuration settings.
 */
class LoginDialogHotkeySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), ['login_hotkey_dialog']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'login_dialog_hotkey.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'login_dialog_hotkey_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('login_dialog_hotkey.settings');

    $form['dialog_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Dialog type'),
      '#description' => $this->t('Select the type of dialog to use.'),
      '#default_value' => $config->get('dialog_type'),
      '#options' => [
        0 => $this->t('modal'),
        1 => $this->t('off-canvas'),
      ],
    ];

    $form['redirect_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Redirect after login'),
      '#description' => $this->t('Choose the redirect destination after logging in.'),
      '#default_value' => $config->get('redirect_type'),
      '#options' => [
        0 => $this->t('no redirect'),
        1 => $this->t('current page'),
        2 => $this->t('other…'),
      ],
    ];

    $form['redirect_destination'] = [
      '#type' => 'textfield',
      '#size' => '60',
      '#placeholder' => '/user',
      '#title' => $this->t('Redirect destination'),
      '#description' => $this->t('Specify the path to redirect to.'),
      '#default_value' => $config->get('redirect_destination'),
      '#states' => [
        'visible' => [
          ':input[name="redirect_type"]' => ['value' => 2],
        ],
        'required' => [
          ':input[name="redirect_type"]' => ['value' => 2],
        ],
      ],
    ];

    $key_combination = [
      '#type' => 'details',
      '#title' => $this->t('Key combination'),
      '#open' => TRUE,
    ];

    $key_combination['keyDisplay'] = [
      '#type' => 'textfield',
      '#size' => '14',
      '#title' => $this->t('Key'),
      '#description' => $this->t('Input the key to use.'),
      '#default_value' => $config->get('key'),
      '#required' => TRUE,
      '#field_suffix' => '<div class="output"><div class="key" data-value=""><span class="label">event.key:</span> <span class="value"></span></div><div class="keyCode" data-value=""><span class="label">event.keyCode:</span> <span class="value"></span></div></div>',
    ];

    $key_combination['key'] = [
      '#type' => 'hidden',
      '#default_value' => $config->get('key'),
    ];

    $key_combination['altKey'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Alt'),
      '#description' => $this->t('Include the Alt key (mac: ⌥ option).'),
      '#default_value' => $config->get('alt_key'),
    ];

    $key_combination['ctrlKey'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Control'),
      '#description' => $this->t('Include the Control key (^).'),
      '#default_value' => $config->get('ctrl_key'),
    ];

    $key_combination['metaKey'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Meta'),
      '#description' => $this->t('Include the Meta key (mac: ⌘ command, win: ⊞ windows).'),
      '#default_value' => $config->get('meta_key'),
    ];

    $key_combination['shiftKey'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Shift'),
      '#description' => $this->t('Include the Shift key (⇧).'),
      '#default_value' => $config->get('shift_key'),
    ];

    $form['key_combination'] = $key_combination;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('redirect_destination') === '/user/logout') {
      $form_state->setErrorByName('redirect_destination', $this->t('You are prohibited from specifying this value.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('login_dialog_hotkey.settings')
      ->set('dialog_type', $form_state->getValue('dialog_type'))
      ->set('redirect_type', $form_state->getValue('redirect_type'))
      ->set('redirect_destination', $form_state->getValue('redirect_destination'))
      ->set('key', $form_state->getValue('key'))
      ->set('alt_key', $form_state->getValue('altKey'))
      ->set('ctrl_key', $form_state->getValue('ctrlKey'))
      ->set('meta_key', $form_state->getValue('metaKey'))
      ->set('shift_key', $form_state->getValue('shiftKey'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

((Drupal, settings) => {

  Drupal.behaviors.loginDialogHotkey = {
    
    attach(context, settings) {

      once('loginDialogHotkey', 'html').forEach(function (element) {

        element.classList.add('loginDialogHotkeyOK');
        
        element.addEventListener('keydown', (event) => {
          
          if (event.code === settings.loginDialogHotkey.key) {
            
            let ok = true;
            const keys = ['altKey', 'ctrlKey', 'metaKey', 'shiftKey'];

            keys.forEach((keyName) => {
              //// if this key IS required and NOT present
              if (ok && settings.loginDialogHotkey[keyName] && !event[keyName]) {
                ok = false;
              }
              //// if this key is NOT required and IS present
              if (ok && !settings.loginDialogHotkey[keyName] && event[keyName]) {
                ok = false;
              }
            });

            if (ok) {

              const options = {};

              switch (settings.loginDialogHotkey.dialogType) {
                case 0:
                  options.dialogType = 'modal';
                  break;
                case 1:
                  options.dialogType = 'dialog';
                  options.dialogRenderer = 'off_canvas';
                  break;
              }

              switch (settings.loginDialogHotkey.redirectType) {
                case 0:
                  options.url = '/user/login';
                  break;
                case 1:
                  options.url = '/user/login?destination=' + location.pathname;
                  break;
                case 2:
                  options.url = '/user/login?destination=' + settings.loginDialogHotkey.redirectDestination;
                  break;
              }

              Drupal.ajax(options).execute();

            }

          }
          
        });

      });
      
    }
    
  };
  
})(Drupal, drupalSettings);

((Drupal) => {

  Drupal.behaviors.keyDisplayInputChange = {
    
    attach(context) {
      
      once('keyDisplayInputChange', 'input[name="keyDisplay"]', context).forEach(function (element) {
        
        const hiddenInput = context.querySelector('input[name="key"]');
        const keyDisplay = context.querySelector('input[name="keyDisplay"]');

        const keyAttr = context.querySelector('.js-form-item-keydisplay .output .key');
        const keyOutput = context.querySelector('.js-form-item-keydisplay .output .key .value');

        const keyCodeAttr = context.querySelector('.js-form-item-keydisplay .output .keyCode');
        const keyCodeOutput = context.querySelector('.js-form-item-keydisplay .output .keyCode .value');

        const ignoreCodes = ['AltGraph', 'AltRight', 'AltLeft', 'CapsLock', 'ControlLeft', 'ControlRight', 'MetaLeft', 'MetaRight', 'NumLock', 'ScrollLock', 'ShiftLeft', 'ShiftRight'];

        element.addEventListener('keydown', (event) => {
          // Inhibit key repeat.
          if (
            !ignoreCodes.includes(event.code)
          ) {
            keyDisplay.value = '';
          }
        });

        element.addEventListener('keyup', (event) => {
          
          if (
            !ignoreCodes.includes(event.code)
          ) {
            hiddenInput.value = event.code;
            element.value = event.code;
            element.select();
            
            keyAttr.setAttribute('data-value', event.key);
            keyOutput.textContent = event.key;
            
            if (typeof event.keyCode !== 'undefined'){
              keyCodeAttr.setAttribute('data-value', event.keyCode);
              keyCodeOutput.textContent = event.keyCode;
            }
          }
        });

        element.addEventListener('click', (event) => {
          element.select();
        });

      });
      
    }
    
  };
  
})(Drupal);

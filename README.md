# Login Dialog Hotkey

Open the "/user/login" form in a (modal|off-canvas) dialog with a key 
combination (hotkey), optionally redirecting after login (no redirect|current 
page|other…).

For a full description of the module, visit the
[project page](https://www.drupal.org/project/login_dialog_hotkey).
